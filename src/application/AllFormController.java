package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

// sanity checks not maintained yet

public class AllFormController {

	/*
	 * ACCUSED
	 */
	
    @FXML
    private AnchorPane accusedPane;

    @FXML
    private ChoiceBox<?> accusedId;

    @FXML
    private ChoiceBox<?> lockupId;

    @FXML
    private TextField Ac_first_name;

    @FXML
    private TextField Ac_father_name;

    @FXML
    private TextField Ac_last_name;

    @FXML
    private ChoiceBox<?> Ac_gender;

    @FXML
    private TextField Ac_age;

    @FXML
    private ChoiceBox<?> Ac_status;

    @FXML
    private TextField Ac_contact_no;

    @FXML
    private ChoiceBox<?> Ac_state;

    @FXML
    private ChoiceBox<?> Ac_city;

    @FXML
    private TextArea Ac_address;

    @FXML
    private TextField Ac_pinnumber;

    @FXML
    private Button Ac_uploadphoto;

    @FXML
    private Button accused_submit;

    @FXML
    void accused_Submit(ActionEvent event) {

    }

    @FXML
    void uploadPhoto(ActionEvent event) {

    }
    
    
    /*
     * VICTIM/PETITIONER
     */
    
    @FXML
    private AnchorPane V_P_Pane;

    @FXML
    private ChoiceBox<?> V_P_Id;

    @FXML
    private TextField V_P_first_name;

    @FXML
    private TextField V_P_father_name;

    @FXML
    private TextField V_P_last_name;

    @FXML
    private ChoiceBox<?> V_P_gender;

    @FXML
    private TextField V_P_age;

    @FXML
    private TextField V_P_contact_no;

    @FXML
    private ChoiceBox<?> V_P_state;

    @FXML
    private ChoiceBox<?> V_P_city;

    @FXML
    private TextArea V_P_address;

    @FXML
    private TextField V_P_pinnumber;

    @FXML
    private Button V_P_submit;

    @FXML
    void victim_Petitioner_Submit(ActionEvent event) {

    }
       
    
    /*
     * OFFICER
     */
   
    @FXML
    private AnchorPane officerPane;

    @FXML
    private ChoiceBox<?> officerId;

    @FXML
    private TextField Of_first_name;

    @FXML
    private TextField Of_father_name;

    @FXML
    private TextField Of_last_name;

    @FXML
    private ChoiceBox<?> Of_gender;

    @FXML
    private TextField Of_contact_no;

    @FXML
    private ChoiceBox<?> Of_state;

    @FXML
    private ChoiceBox<?> Of_city;

    @FXML
    private TextArea Of_address;

    @FXML
    private TextField Of_pinnumber;

    @FXML
    private Button officer_submit;

    @FXML
    private TextField Of_rank;

    @FXML
    private DatePicker Of_joining_date;

    @FXML
    private TextField Of_shift;

    @FXML
    private DatePicker Of_dob;

    @FXML
    private Button Of_photo;

    @FXML
    void Of_uploadPhoto(ActionEvent event) {

    }

    @FXML
    void officer_Submit(ActionEvent event) {

    }
}
