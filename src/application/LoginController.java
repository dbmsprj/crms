package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class LoginController {

	@FXML
	private Pane pane;

	@FXML
	private TextField govt_id;

	@FXML
	private PasswordField password;

	@FXML
	private Button loginbtn;

	@FXML
	private Label errorlabel;

	@FXML
	void LoginAction(ActionEvent event) throws Exception {
		String gvt_id = govt_id.getText();
		String pass = password.getText();
		Connection c = Constants.getConnection();
		if (c != null) {
			Statement s = c.createStatement();
			String query = "select username, password  from login where username ='" + gvt_id + "'";
			ResultSet result = s.executeQuery(query);
			if (!result.isBeforeFirst()) {
				errorlabel.setText("Incorrect Credentials!");
				errorlabel.setTextFill(Color.RED);
			} else {
				result.first();
				if (pass.equals(result.getString("password"))) {
					pane.getScene().getWindow().hide();
					Stage primaryStage = new Stage();
					try {
						AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("firstpage.fxml"));
						Scene scene = new Scene(page);
						primaryStage.setScene(scene);
						primaryStage.setTitle("CRIMINAL RECORD MANAGEMENT PORTAL");
						primaryStage.show();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					errorlabel.setText("Incorrect Credentials!");
					errorlabel.setTextFill(Color.RED);
				}
			}
		}

		// if(gvt_id.equals("aish") && pass.equals("aish")){
		// pane.getScene().getWindow().hide();
		// Stage primaryStage = new Stage();
		// try{
		// AnchorPane page = (AnchorPane)
		// FXMLLoader.load(Main.class.getResource("firstpage.fxml"));
		// Scene scene = new Scene(page);
		// primaryStage.setScene(scene);
		// primaryStage.setTitle("CRIMINAL RECORD MANAGEMENT PORTAL");
		// primaryStage.show();
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
		//
		// }
		// else{
		// errorlabel.setText("Incorrect Credentials!");
		// errorlabel.setTextFill(Color.RED);
		// }

	}

}
