package application;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class FirstPageController {

	@FXML
	private Button addcase;

	@FXML
	private Button addfir;

	@FXML
	private Button addvictim;

	@FXML
	private Button addcriminal;

	@FXML
	private Button addpetitioner;
	@FXML
	private Button addofficer;

	// @FXML
	// private Button updatecase;
	@FXML
	private Button caseoutcome;
	@FXML
	private Button top10_crimi_local;
	@FXML
	private Button Number_cases_in_locality;
	@FXML
	private Button max_case_locality;
	@FXML
	private Button min_crime_locality;
	@FXML
	private Button frequent_crime;
	@FXML
	private Button count_type_crime;
	@FXML
	private Button court_max_pending_cases;
	@FXML
	private Button top_10_invest_off;
	@FXML
	private Button cases_accused_criminal;
	@FXML
	private Button locality_max_criminals;
	@FXML
	private Button criminal_place_not_known;
	@FXML
	private Button court_max_cases_solved;
	@FXML
	private Button all_pending_cases;
	
	@FXML
    private Button viewAccused;

    @FXML
    private Button viewVictim;

    @FXML
    private Button viewOfficer;

    @FXML
    private Button viewPetitioner;

	@FXML
	void top10_crimi_local_action(ActionEvent event) {
		JFrame frame = new JFrame("Query Input");

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JLabel label = new JLabel("Enter Pin No.:");
		JTextField text = new JTextField(5);
		JButton button = new JButton();
		button.setText("Submit");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String t = text.getText().toString();
				System.out.println(t);

				String query = "select A.accusedID as accusedID, P.first_name as CriminalName, count(*) as No_of_Crimes from cases as C join accused as A join person as P where P.aadharID = A.aadharID and A.accusedID = C.accusedID and A.status = 'Guilty' and P.pin ='"
						+ t + "'   group by accusedID   order by No_of_Crimes desc limit 10";
				try {
					getQueryResult(query);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				frame.dispose();

			}
		});
		// button.addActionListener();
		panel.add(label);
		panel.add(text);
		panel.add(button);

		frame.add(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

	String get_formattedDate(JDatePickerImpl d)
	{
		System.out.println(d.getModel().getValue() +" "+ (d.getModel().getMonth()+1));
		return d.getModel().getYear() + "-" + (d.getModel().getMonth()+1) +"-" + d.getModel().getDay();
	}
	
	@FXML
	void Number_cases_in_locality_action(ActionEvent event) {
		JFrame frame = new JFrame("Query Input");
		JPanel panel = new JPanel();
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		panel.setLayout(new GridLayout(3,2));
		panel1.setLayout(new FlowLayout());
		panel2.setLayout(new GridLayout(2,1));
		JLabel label = new JLabel("Enter Pin No.:");
		JLabel label1 = new JLabel("Start date");
		JLabel label2 = new JLabel("End date");
		UtilDateModel model = new UtilDateModel();
		UtilDateModel model1 = new UtilDateModel();
		Properties p = new Properties();

		JDatePanelImpl datePanel_s = new JDatePanelImpl(model,p);
		JDatePanelImpl datePanel_e = new JDatePanelImpl(model1,p);
		
		JDatePickerImpl datePicker_start = new JDatePickerImpl(datePanel_s,new DateLabelFormatter());
		JDatePickerImpl datePicker_end = new JDatePickerImpl(datePanel_e, new DateLabelFormatter());
		JTextField text = new JTextField(5);
		JButton button = new JButton();
		button.setText("Submit");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String t = text.getText().toString();	
				String start_Date = get_formattedDate(datePicker_start);
				String end_Date = get_formattedDate(datePicker_end);
				System.out.println(start_Date +" " + end_Date); 
				String query = "select fir.firNumber, File_Date, Court, first_name as officerName, place_of_Incidence, Time_of_Incidence, details from person natural join investigating_officer natural join cases join fir where fir.firNumber = cases.firNumber and File_date between '"+start_Date+"' and '"+end_Date+"'  and fir.pin="+t+"";
				System.out.println(query);
				try {
					getQueryResult(query);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				frame.dispose();
			}
		});
		panel.add(label);
		panel.add(text);
		panel.add(label1);
		panel.add(datePicker_start);
		panel.add(label2);
		panel.add(datePicker_end);
		panel1.add(button);
		panel2.add(panel);
		panel2.add(panel1);
		frame.add(panel2);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);	}

	@FXML
	void max_case_locality_action(ActionEvent event) {
		String query = "select pin,count(*) as Count from fir natural join cases group by pin order by Count desc limit 1";
		System.out.println(query);
		try {
			getQueryResult(query);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void min_crime_locality_action(ActionEvent event) {
		String query = "select pin,count(*) as Count from fir natural join cases natural join case_outcome where case_outcome.status = 'Guilty' group by pin order by Count limit 1";
		try {
			getQueryResult(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void frequent_crime_action(ActionEvent event) {
		String query = " select type as Type_of_Crime ,count(*) as Count from cases natural join case_outcome where case_outcome.status = 'Guilty' group by Type_of_Crime order by Count desc limit 1";
		try {
			getQueryResult(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void count_type_crime_action(ActionEvent event) {
		JFrame frame = new JFrame("Query Input");

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JLabel label = new JLabel("Enter Type of Crime:");
		JTextField text = new JTextField(5);
		JButton button = new JButton();
		button.setText("Submit");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String t = text.getText().toString();
				System.out.println(t);
				String query = "select type as Type_of_Crime, count(*) as Count from cases natural join case_outcome where case_outcome.status = 'Guilty' group by Type_of_Crime having Type_of_Crime = '"+t+"'";
				try {
					getQueryResult(query);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				frame.dispose();

			}
		});
		panel.add(label);
		panel.add(text);
		panel.add(button);

		frame.add(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}
	
	@FXML
	void court_max_pending_cases_action(ActionEvent event) {
		String query = "select court as Court , count(*) as No_of_Pending_Cases from cases where caseID not in (select caseID from case_outcome) group by Court order by No_of_Pending_Cases desc limit 1";
		try {
			getQueryResult(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void top_10_invest_off_action(ActionEvent event) {
		String query = "select P.first_name as officerName, investigating_officer.ioID, count(*) as Count from person as P natural join investigating_officer natural join cases group by investigating_officer.ioID, officerName order by Count desc";
		try {
			getQueryResult(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void cases_accused_criminal_action(ActionEvent event) {
		String query = "select  T.first_name as accusedName, Q.details as caseDetails from (select accusedID, first_name from accused natural join person where status = 'Guilty') as T natural join (select * from cases where caseID not in (select caseID from case_outcome)) as Q";
		try {
			getQueryResult(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void locality_max_criminals_action(ActionEvent event) {
		String query = "select pin, count(*) as Count from accused natural join person where status = 'Guilty' group by pin order by Count desc limit 1";
		try {
			getQueryResult(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void criminal_place_not_known_action(ActionEvent event) {
		String query = "select count(*) as Count from accused natural join person where status = 'Guilty' and pin is null";
		try {
			getQueryResult(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void court_max_cases_solved_action(ActionEvent event) {
		String query = "select court, count(*) as Count from cases natural join case_outcome group by court order by Count desc limit 1";
		try {
			getQueryResult(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void all_pending_cases_action(ActionEvent event) {
		JFrame frame = new JFrame("Query Input");

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JLabel label = new JLabel("Enter Pin No.:");
		JTextField text = new JTextField(5);
		JButton button = new JButton();
		button.setText("Submit");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String t = text.getText().toString();
				System.out.println(t);

				String query = "select * from cases natural join fir where caseID not in (select caseID from case_outcome) and pin = "+t+"";
				System.out.println(query);
				try {
					getQueryResult(query);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				frame.dispose();

			}
		});
		panel.add(label);
		panel.add(text);
		panel.add(button);

		frame.add(panel);
		frame.setSize(150, 130);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	@FXML
	void addCase(ActionEvent event) {
		Stage primaryStage = new Stage();
		try {
			AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("caseform.fxml"));
			Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			primaryStage.setTitle("CRIMINAL RECORD MANAGEMENT PORTAL");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void addCriminal(ActionEvent event) {
		Stage primaryStage = new Stage();
		try {
			AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("accusedform.fxml"));
			Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			primaryStage.setTitle("CRIMINAL RECORD MANAGEMENT PORTAL");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void addFir(ActionEvent event) {
		Stage primaryStage = new Stage();
		try {
			AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("firform.fxml"));
			Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			primaryStage.setTitle("CRIMINAL RECORD MANAGEMENT PORTAL");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void addOfficer(ActionEvent event) {
		Stage primaryStage = new Stage();
		try {
			AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("officerform.fxml"));
			Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			primaryStage.setTitle("CRIMINAL RECORD MANAGEMENT PORTAL");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void addPetitioner(ActionEvent event) {
		Stage primaryStage = new Stage();
		try {
			Constants.Vic = false;
			AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("victim_Petitioner_form.fxml"));
			Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			primaryStage.setTitle("CRIMINAL RECORD MANAGEMENT PORTAL");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void addVictim(ActionEvent event) {
		Stage primaryStage = new Stage();
		try {
			Constants.Vic = true;
			AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("victim_Petitioner_form.fxml"));
			Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			primaryStage.setTitle("CRIMINAL RECORD MANAGEMENT PORTAL");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getQueryResult(String query) throws Exception {

		Connection c = Constants.getConnection();
		try {
			if (c != null) {
				Statement s = c.createStatement();
				ResultSet result = s.executeQuery(query);
				result.beforeFirst();

				ResultSetMetaData resultMetaData = result.getMetaData();
				Object[] columns = new String[resultMetaData.getColumnCount()];

				result.last();
				Object[][] data = new Object[result.getRow()][resultMetaData.getColumnCount()];
				result.beforeFirst();

				int j = 0;
				for (int i = 1; i <= resultMetaData.getColumnCount(); i++) {
					columns[j++] = new String(resultMetaData.getColumnLabel(i));
				}
				int row_count = 0;
				while (result.next()) {
					int col_iter = 0;
					for (int i = 1; i <= resultMetaData.getColumnCount(); i++) {
						data[row_count][col_iter++] = new String(result.getString(i));
					}
					row_count++;
				}

				JFrame frame = new JFrame("Queried Output");

				JTable table = new JTable(data, columns);
				table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				Container contentPane = frame.getContentPane();

			    table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 15));
				table.setRowHeight(30);
				JPanel topPanel = new JPanel();
				topPanel.setLayout( new BorderLayout() );
				contentPane.add( topPanel );
				for(int i = 0 ; i < table.getColumnCount(); i++){
					table.getColumnModel().getColumn(i).setPreferredWidth(175);;
	
				}
				
				table.setShowHorizontalLines( false );
				table.setRowSelectionAllowed( true );
				table.setColumnSelectionAllowed( true );

				table.setSelectionForeground( Color.white );
				table.setSelectionBackground( Color.red );

				JScrollPane scrollPane = table.createScrollPaneForTable( table );
				topPanel.add( scrollPane, BorderLayout.CENTER );
				
//				frame.setLocationRelativeTo(null);
				frame.setSize(800,500);
				frame.setVisible(true);
			}

		} catch (SQLException e) {
			JFrame frame = new JFrame("Data Base Error");
			JPanel panel = new JPanel();
			panel.setLayout(new FlowLayout());

			JLabel label = new JLabel("Error: Input not present in DataBase!", JLabel.CENTER);
			label.setForeground(Color.red);
			panel.add(label);
			frame.add(panel);
			frame.setLocationRelativeTo(null);
			frame.pack();
			frame.setVisible(true);

		}
	}

	@FXML
	void caseOutcome(ActionEvent event) {
		Stage primaryStage = new Stage();
		try {
			AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("case_outcome.fxml"));
			Scene scene = new Scene(page);
			primaryStage.setScene(scene);
			primaryStage.setTitle("CRIMINAL RECORD MANAGEMENT PORTAL");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void viewResult(String query) throws Exception {

		Connection c = Constants.getConnection();
		try {
			if (c != null) {
				Statement s = c.createStatement();
				ResultSet result = s.executeQuery(query);
				ResultSetMetaData resultMetaData = result.getMetaData();
							
				JFrame frame = new JFrame("Queried Output");
//				Container contentPane = frame.getContentPane();
				JPanel topPanel = new JPanel();
				topPanel.setLayout(null);
				int lX = 100, vX = 260;
				int lY = 100;
				JLabel heading = new JLabel("RESULT", JLabel.CENTER);
				heading.setFont(new Font("Garamond", Font.BOLD, 20));
				topPanel.add(heading);
				heading.setSize(300, 30);
				heading.setLocation(150, 20);
				
				JLabel[] label = new JLabel[resultMetaData.getColumnCount()];
				JLabel[] value = new JLabel[resultMetaData.getColumnCount()];
				for (int i = 1; i <= resultMetaData.getColumnCount(); i++) {
					result.first();
					label[i-1] = new JLabel( new String(resultMetaData.getColumnLabel(i)));
					label[i-1].setSize(200, 30);
					label[i-1].setFont(new Font("Times New Roman", Font.BOLD, 13));
					label[i-1].setLocation(lX, lY);
					topPanel.add(label[i-1]);
					
					value[i-1] = new JLabel(result.getString(label[i-1].getText()).toString());
					value[i-1].setLocation(vX, lY);
					value[i-1].setSize(200, 30);
					value[i-1].setFont(new Font("Times New Roman", Font.PLAIN, 13));
					lY += 40;
					
					topPanel.add(value[i-1]);
				}				
				frame.add( topPanel );
				
				frame.setLocationRelativeTo(null);
				frame.setSize(600,700);
				frame.setVisible(true);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			JFrame frame = new JFrame("Data Base Error");
			JPanel panel = new JPanel();
			panel.setLayout(new FlowLayout());

			JLabel label = new JLabel("Error: Input not present in DataBase!", JLabel.CENTER);
			label.setForeground(Color.red);
			panel.add(label);
			frame.add(panel);
			frame.setLocationRelativeTo(null);
			frame.pack();
			frame.setVisible(true);

		}
	}
	
	
	@FXML
    void viewAccused(ActionEvent event) {
		JFrame frame = new JFrame("Query Input");

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JLabel label = new JLabel("Enter AccusedID:");
		JTextField text = new JTextField(5);
		JButton button = new JButton();
		button.setText("Submit");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String t = text.getText().toString();
				System.out.println(t);

				String query = "select * from person natural join accused where accusedID = "+t+"";
				System.out.println(query);
				try {
					viewResult(query);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				frame.dispose();

			}
		});
		panel.add(label);
		panel.add(text);
		panel.add(button);

		frame.add(panel);
		frame.setSize(150, 130);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

    }

    @FXML
    void viewOfficer(ActionEvent event) {
    	JFrame frame = new JFrame("Query Input");

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JLabel label = new JLabel("Enter Officer ID.:");
		JTextField text = new JTextField(5);
		JButton button = new JButton();
		button.setText("Submit");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String t = text.getText().toString();
				System.out.println(t);

				String query = "select * from person natural join investigating_officer where ioID = "+t+"";
				System.out.println(query);
				try {
					viewResult(query);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				frame.dispose();

			}
		});
		panel.add(label);
		panel.add(text);
		panel.add(button);

		frame.add(panel);
		frame.setSize(150, 130);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

    }

    @FXML
    void viewPetitioner(ActionEvent event) {
    	JFrame frame = new JFrame("Query Input");

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JLabel label = new JLabel("Enter Petitioner ID:");
		JTextField text = new JTextField(5);
		JButton button = new JButton();
		button.setText("Submit");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String t = text.getText().toString();
				System.out.println(t);

				String query = "select * from person natural join petitioner where petitionerID = "+t+"";
				System.out.println(query);
				try {
					viewResult(query);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				frame.dispose();

			}
		});
		panel.add(label);
		panel.add(text);
		panel.add(button);

		frame.add(panel);
		frame.setSize(150, 130);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

    }

    @FXML
    void viewVictim(ActionEvent event) {
    	JFrame frame = new JFrame("Query Input");

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JLabel label = new JLabel("Enter Victim ID:");
		JTextField text = new JTextField(5);
		JButton button = new JButton();
		button.setText("Submit");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String t = text.getText().toString();
				System.out.println(t);

				String query = "select * from person natural join victim where victimID = "+t+"";
				System.out.println(query);
				try {
					viewResult(query);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				frame.dispose();

			}
		});
		panel.add(label);
		panel.add(text);
		panel.add(button);

		frame.add(panel);
		frame.setSize(150, 130);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

    }


}
