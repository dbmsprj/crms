package application;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class CaseController implements Initializable {

	@FXML
	private AnchorPane casePane;

	@FXML
	private TextField in_Off;

	@FXML
	private TextField fir_Number;

	@FXML
	private TextArea case_Description;

	@FXML
	private Button case_submit;

	@FXML
	private Label warning;
	
    @FXML
    private TextField accusedID;

    @FXML
    private TextField crimeType;

    @FXML
    private ChoiceBox<String> highCourt;

	@FXML
	void case_Submit(ActionEvent event) throws Exception {
		String invest_Off = in_Off.getText();
		String fir_No = fir_Number.getText();
		String cDesc = case_Description.getText();
		String type = crimeType.getText();
		String court = highCourt.getSelectionModel().getSelectedItem();
		String accused = accusedID.getText();
		
		Connection c = Constants.getConnection();
		if (c != null) {
			Statement s = c.createStatement();
			String query = "select firNumber from fir where firNumber=" + fir_No+ "";
			System.out.println(query);
			ResultSet result = s.executeQuery(query);
			boolean flag = true;
			if (!result.isBeforeFirst()) {
				warning.setText("No such Fir filed yet.Please check your database");
				flag = false;
			}
			if (flag) {
				query = "Insert into cases(caseID,ioID,firNumber,accusedID,type,court,details) values('" + Constants.caseID++ + "','"
						+ invest_Off + "','" + fir_No+ "','" +accused + "','" + type + "','" + court+ "','" + cDesc + "')";
				System.out.println(query);
				s.execute(query);
				Constants.write();
			}
		}
		casePane.getScene().getWindow().hide();
	}

	@Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
		if(Constants.highCourts == null) System.out.println("null");
		highCourt.setItems(Constants.highCourts);
		highCourt.getSelectionModel().selectFirst();
	}

}
