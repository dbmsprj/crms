package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Constants {
	// public static Connection connection = DriverManager
	public static Connection getConnection() throws Exception {
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/crms1", "root", "Natural_96");
		return connection;
	}

	public static ObservableList<String> states = FXCollections.observableArrayList();

	public static ObservableList<String> cities = FXCollections.observableArrayList();

	public static ObservableList<String> highCourts = FXCollections.observableArrayList();
	private static Connection c = null;

	public static boolean Vic;
	
	static {
		try {
			c = DriverManager.getConnection("jdbc:mysql://localhost/HighCourts", "root", "Natural_96");
			Statement s = c.createStatement();
			String query = "Select distinct(court) from highCourt";
			ResultSet result = s.executeQuery(query);

			highCourts.add("-");
			while (result.next())
				highCourts.add(result.getString("court"));

			c = DriverManager.getConnection("jdbc:mysql://localhost/states_cities_list", "root", "Natural_96");
			s = c.createStatement();
			query = "Select distinct(state) from india_states_cities";
			result = s.executeQuery(query);

			states.add("-");
			while (result.next())
				states.add(result.getString("state"));
	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void cities(String state) {
		System.out.println(state);
		if (c != null && !state.equals("-")) {
			try {
				Statement s = c.createStatement();
				String query = "Select distinct(city) from india_states_cities where state ='" + state + "'";
				System.out.println(query);
				ResultSet result = s.executeQuery(query);
				cities.clear();
				cities.add("-");
				while (result.next())
					cities.add(result.getString("city"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static Integer accusedID;
	public static Integer victimID;
	public static Integer IO_ID;
	public static Integer petitionerID;
	public static Integer firNumber, caseID, caseOutcomeID;
	static {
		try {

			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader("t.txt"));
				String[] list = reader.readLine().split(",");
				accusedID = Integer.parseInt(list[0]);
				System.out.println(accusedID);
				victimID = Integer.parseInt(list[1]);
				System.out.println(victimID);
				IO_ID = Integer.parseInt(list[2]);
				System.out.println(IO_ID);
				petitionerID = Integer.parseInt(list[3]);
				System.out.println(petitionerID);
				firNumber = Integer.parseInt(list[4]);
				caseID = Integer.parseInt(list[5]);
				System.out.println(firNumber);
				caseOutcomeID = Integer.parseInt(list[6]);
				
				reader.close();
			} catch (Exception e) {
//				e.printStackTrace();
				accusedID = 1;
				victimID = 1;
				IO_ID = 1;
				petitionerID = 1;
				firNumber = 1;
				caseID = 1;
				caseOutcomeID = 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void write() {
		try {
			File file = new File("t.txt");
			if (!file.exists()) {
				file.createNewFile();
			}
			BufferedWriter writer = null;
			writer = new BufferedWriter(new FileWriter(file));
			writer.write("" + accusedID + "," + victimID + "," + IO_ID + "," + petitionerID+ "," + firNumber +","+ caseID+","+ caseOutcomeID);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ObservableList<String> gender = FXCollections.observableArrayList("-", "F", "M");

	public static ObservableList<String> ac_status = FXCollections.observableArrayList("-", "Innocent", "Guilty");

}
