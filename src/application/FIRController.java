package application;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class FIRController {

	@FXML
	private AnchorPane firPane;

	@FXML
	private TextField petitionerName;

	@FXML
	private TextField vicName;

	@FXML
	private DatePicker dateFile;

	@FXML
	private DatePicker dateIncident;

	@FXML
	private TextField timeIncident;

	@FXML
	private TextField placeIncident;

	@FXML
	private Button fir_submit;

	@FXML
	private TextField pinNumber;

	@FXML
	private TextField timeFile;

	@FXML
	private Label warning;

	@FXML
	void fir_Submit(ActionEvent event) throws Exception {
		String petitionerID = petitionerName.getText();
		String victimID = vicName.getText();
		LocalDate fileDate = dateFile.getValue();
		String fileTime = timeFile.getText();
		LocalDate dateInc = dateIncident.getValue();
		String timeInc = timeIncident.getText();
		String placeInc = placeIncident.getText();
		String pin = pinNumber.getText();

		Connection c = Constants.getConnection();
		if (c != null) {
			Statement s = c.createStatement();
			String query = "select petitionerID from petitioner where petitionerID=" + petitionerID + "";
			ResultSet result = s.executeQuery(query);
			boolean flag = true;
			if (!result.isBeforeFirst()) {
				warning.setText("Petitioner ID not in database. Please fill in required data first");
				flag = false;
			}
			query = "select victimID from victim where victimID=" + victimID + "";
			result = s.executeQuery(query);
			if (!result.isBeforeFirst()) {
				warning.setText("Victim ID not in database. Please fill in required data first");
				flag = false;
			}
			if (flag) {
				query = "Insert into fir(firNumber,petitionerID,victimID,File_date,File_time,Date_of_Incidence,Time_of_Incidence,place_of_Incidence,pin) values('"
						+ Constants.firNumber++ + "','" + petitionerID + "','" + victimID + "','" + fileDate + "','"
						+ fileTime + "','" + dateInc + "','" + timeInc + "','" + placeInc + "','" + pin + "')";
				System.out.println(query);
				s.execute(query);
				Constants.write();
			}
		}
		firPane.getScene().getWindow().hide();

	}

}
