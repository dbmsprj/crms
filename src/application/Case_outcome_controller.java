package application;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class Case_outcome_controller implements Initializable {

	@FXML
	private AnchorPane C_O_pane;

	@FXML
	private TextField caseID;

	@FXML
	private TextArea otherDetails;

	@FXML
	private Button case_outcome_button;

	@FXML
	private TextField S_o_L;

	@FXML
	private ChoiceBox<String> guilty;

	@FXML
	private TextField Judgement;
	
	@FXML
	private Label warning;

	@FXML
	void case_out_action(ActionEvent event) throws Exception {
		String caseId = caseID.getText();
		String Status = guilty.getSelectionModel().getSelectedItem();
		String details = otherDetails.getText();
		String judgement = Judgement.getText();
		String sol = S_o_L.getText();

		Connection c = Constants.getConnection();
		if (c != null) {
			Statement s = c.createStatement();
			String query = "select caseID from cases where caseID = " + caseId+ "";
			System.out.println(query);
			ResultSet result = s.executeQuery(query);
			boolean flag = true;
			if (!result.isBeforeFirst()) {
				warning.setText("No such Case filed yet.Please check your database");
				flag = false;
			}
			query = "select solID from section_of_law where solID ='" + sol+"'";
			System.out.println(query);
			result = s.executeQuery(query);
			if(!result.isBeforeFirst())
			{
				warning.setText("No such Article in database. Please recheck");
				flag = false;
			}
			if (flag) {
				query = "Insert into case_outcome(CO_id,caseID,solID,status,judgement, otherDetails) values('"
						+ Constants.caseOutcomeID++ + "','" + caseId + "','" + sol + "','" + Status + "','" + judgement
						+ "','" + details + "')";
				System.out.println(query);
				s.execute(query);
				Constants.write();
				query = "select accused.status as status , accused.accusedID as accusedID from accused join cases join case_outcome where cases.accusedID = accused.accusedID and cases.caseID =  case_outcome.caseID and case_outcome.caseID = "+caseId+"";
				System.out.println(query);
				result = s.executeQuery(query);
				result.first();
				String accusedID = result.getString("accusedID");
				if(Status.equals("Guilty") && !result.getString("status").equals("Guilty"))
				{
					query = "update accused set accused.status = 'Guilty' where accusedID = "+ accusedID+"";
					System.out.println(query);
					s.executeUpdate(query);
				}
			}
		}
		C_O_pane.getScene().getWindow().hide();
	}

	@Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
		guilty.setItems(Constants.ac_status);
		guilty.getSelectionModel().selectFirst();

	}

}
