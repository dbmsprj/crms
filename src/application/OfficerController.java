package application;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class OfficerController implements Initializable {

	@FXML
    private AnchorPane officerPane;

    @FXML
    private TextField Of_first_name;

    @FXML
    private TextField Of_last_name;

    @FXML
    private ChoiceBox<String> Of_gender;

    @FXML
    private TextField Of_contact_no;

    @FXML
    private ChoiceBox<String> Of_city;

    @FXML
    private TextArea Of_address;

    @FXML
    private TextField Of_pinnumber;

    @FXML
    private Button officer_submit;

    @FXML
    private TextField Of_rank;

    @FXML
    private DatePicker Of_joining_date;

    @FXML
    private TextField Of_shift;

    @FXML
    private DatePicker Of_dob;

//    @FXML
//    private Button Of_photo;

    @FXML
    private TextField officerId;

    @FXML
    private ComboBox<String> Of_state;
    
    @FXML
    private TextField Of_fatherName;

//    @FXML
//    void Of_uploadPhoto(ActionEvent event) {
//
//    }

    @FXML
    void getCity(ActionEvent event) {
    	String state = Of_state.getSelectionModel().getSelectedItem();
    	Constants.cities(state);
    	Of_city.setItems(Constants.cities);
    	Of_city.getSelectionModel().selectFirst();
    }

    @FXML
    void officer_Submit(ActionEvent event) throws Exception {
    	String aadhar = officerId.getText();
    	String fstName = Of_first_name.getText();
    	String lstName = Of_last_name.getText();
    	String gender = Of_gender.getSelectionModel().getSelectedItem();
    	String contact = Of_contact_no.getText();
    	String father = Of_fatherName.getText();
    	String rank = Of_rank.getText();
    	String posted_station = Of_shift.getText();
    	LocalDate joiningDate = Of_joining_date.getValue();
    	String state = Of_state.getSelectionModel().getSelectedItem();
    	String city = Of_city.getSelectionModel().getSelectedItem();
    	String addr = Of_address.getText();
    	String pin = Of_pinnumber.getText();
    	LocalDate dob = Of_dob.getValue();
    	LocalDate currentdate = LocalDate.now();
    	int age = currentdate.getYear() - dob.getYear();  
    	System.out.println("age "+age);
    	Connection c = Constants.getConnection();
    	if (c != null){
	    	Statement s = c.createStatement();	
	    	String query = "select aadharID from person where aadharID ="+aadhar+"";
	    	System.out.println(query);
	    	ResultSet result = s.executeQuery(query);
	    	
	    	if(!result.isBeforeFirst()){
	    		System.out.println("already doesnt exist");
	    		query = "Insert into person(aadharID,first_name,last_name,father_name,gender,age,contact_no,state,city,address,pin) values('"+aadhar +"','"+ fstName +"','"+ lstName +"','"+ father+"','"+ gender +"','"+age +"','"+contact +"','"+state+"','"+city+"','"+addr+"','"+pin+"')";
	    		s.execute(query);
	    	}
	    	System.out.println("yoooooooooooo");
	    	query = "Insert into investigating_officer(ioID,aadharID,rank,posted_station,joining_date,DOB) values('"+ Constants.IO_ID++ +"','"+aadhar+ "','"+ rank +"','"+ posted_station + "','"+ joiningDate+"','"+ dob+"')";
	    	s.execute(query);
			Constants.write();
		}
    	officerPane.getScene().getWindow().hide();
    }    
    @Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
    	
    	Of_gender.setItems(Constants.gender);
    	Of_state.setItems(Constants.states);
    	
		Of_gender.getSelectionModel().selectFirst();
		Of_state.getSelectionModel().selectFirst();
	}

}
