package application;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class CriminalController implements Initializable {

	@FXML
    private AnchorPane accusedPane;

    @FXML
    private TextField Ac_first_name;

    @FXML
    private TextField Ac_father_name;

    @FXML
    private TextField Ac_last_name;

    @FXML
    private ChoiceBox<String> Ac_gender;

    @FXML
    private TextField Ac_age;

    @FXML
    private TextField Ac_contact_no;

    @FXML
    private ChoiceBox<String> Ac_city;

    @FXML
    private TextArea Ac_address;

    @FXML
    private TextField Ac_pinnumber;

//    @FXML
//    private Button Ac_uploadphoto;

    @FXML
    private Button accused_submit;

    @FXML
    private TextField Ac_aadharID;

    @FXML
    private ComboBox<String> Ac_state;

    @FXML
    void getCitites(ActionEvent event) {
    	String state = Ac_state.getSelectionModel().getSelectedItem();
    	Constants.cities(state);
    	Ac_city.setItems(Constants.cities);
    	Ac_city.getSelectionModel().selectFirst();    	
    }

	@FXML
    void accused_Submit(ActionEvent event)throws Exception {
    	String pin = Ac_pinnumber.getText();
    	String address = Ac_address.getText();
    	String city = Ac_city.getSelectionModel().getSelectedItem();
    	String contact = Ac_contact_no.getText();
    	String status = "Innocent";
    	String age = Ac_age.getText();
    	String gender = Ac_gender.getSelectionModel().getSelectedItem();
    	String lstName = Ac_last_name.getText();
    	String fstName = Ac_first_name.getText();
    	String aadharId = Ac_aadharID.getText();
    	String father = Ac_father_name.getText();
    	String state = Ac_state.getSelectionModel().getSelectedItem();
    	
    	Connection c = Constants.getConnection();
    	if (c != null){
	    	Statement s = c.createStatement();
	    	System.out.println(Constants.accusedID);
	    	String query = "select aadharID from person where aadharID ="+aadharId+"";
	    	ResultSet result = s.executeQuery(query);
	    	if(!result.isBeforeFirst()){
	    		System.out.println("already doesnt exist");
	    		query = "Insert into person(aadharID,first_name,last_name,father_name,gender,age,contact_no,state,city,address,pin) values('"+aadharId +"','"+ fstName +"','"+ lstName +"','"+ father +"','"+ gender +"','"+age +"','"+contact +"','"+state+"','"+city+"','"+address+"','"+pin+"')";
	    		s.execute(query);
	    	}
	    	query = "Insert into accused(accusedID,aadharID,status) values('"+ Constants.accusedID++ + "','"+aadharId +"','"+status+"')";
	    	s.execute(query);
	    	Constants.write();
		}
    	accusedPane.getScene().getWindow().hide();
		
    }

//	@FXML
//	void uploadPhoto(ActionEvent event) {
//
//	}

	@Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resources) {

		Ac_gender.setItems(Constants.gender);
		Ac_state.setItems(Constants.states);
//		Ac_city.setItems(Constants.cities);
		
		Ac_gender.getSelectionModel().selectFirst();
		Ac_state.getSelectionModel().selectFirst();
//		Ac_city.getSelectionModel().selectFirst();
	}

}
