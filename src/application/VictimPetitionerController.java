package application;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class VictimPetitionerController implements Initializable {

	@FXML
    private AnchorPane V_P_Pane;

    @FXML
    private TextField V_P_first_name;

    @FXML
    private TextField V_P_father_name;

    @FXML
    private TextField V_P_last_name;

    @FXML
    private ChoiceBox<String> V_P_gender;

    @FXML
    private TextField V_P_age;

    @FXML
    private TextField V_P_contact_no;

    @FXML
    private ChoiceBox<String> V_P_city;

    @FXML
    private TextArea V_P_address;

    @FXML
    private TextField V_P_pinnumber;

    @FXML
    private Button V_P_submit;

    @FXML
    private TextArea V_P_condition;

    @FXML
    private TextField V_P_Id;

    @FXML
    private ComboBox<String> V_P_state;

    @FXML
    void getCity(ActionEvent event) {
    	String state = V_P_state.getSelectionModel().getSelectedItem();
    	Constants.cities(state);
    	V_P_city.setItems(Constants.cities);
    	V_P_city.getSelectionModel().selectFirst();
    }

    @FXML
    void victim_Petitioner_Submit(ActionEvent event) throws Exception {
    	String aadharID = V_P_Id.getText();
    	String fstName = V_P_first_name.getText();
    	String lstName = V_P_last_name.getText();
    	String father = V_P_father_name.getText();
    	String gender = V_P_gender.getSelectionModel().getSelectedItem();
    	String age = V_P_age.getText();
    	String contact = V_P_contact_no.getText();
    	String condn = "";
    	if(Constants.Vic) 
    		condn = V_P_condition.getText();
       	String state = V_P_state.getSelectionModel().getSelectedItem();
    	String city = V_P_city.getSelectionModel().getSelectedItem();
    	String addr = V_P_address.getText();
    	String pin = V_P_pinnumber.getText();
    	
    	Connection c = Constants.getConnection();
    	if (c != null){
	    	Statement s = c.createStatement();
	    	String query = "select aadharID from person where aadharID ="+aadharID+"";
	    	ResultSet result = s.executeQuery(query);
	    	if(!result.isBeforeFirst()){
	    		System.out.println("already doesnt exist");
	    		query = "Insert into person(aadharID,first_name,last_name,father_name,gender,age,contact_no,state,city,address,pin) values('"+aadharID +"','"+ fstName +"','"+ lstName +"','"+ father +"','"+ gender +"','"+age +"','"+contact +"','"+state+"','"+city+"','"+addr+"','"+pin+"')";
	    		s.execute(query);
	    	}
	    	if(Constants.Vic)
	    		query = "Insert into victim(victimID,aadharID,condn) values('"+ Constants.victimID++ + "','"+aadharID +"','"+ condn+"')";
	    	else 
	    		query = "Insert into petitioner(petitionerID,aadharID) values('"+ Constants.petitionerID++ + "','"+aadharID+ "')";
	    	System.out.println(query);
			s.execute(query);
			Constants.write();
		}
    	V_P_Pane.getScene().getWindow().hide();
		
    }
    @Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
    	
    	V_P_gender.setItems(Constants.gender);
    	V_P_state.setItems(Constants.states);
    	if(!Constants.Vic) V_P_condition.setDisable(true);
    	else V_P_condition.setEditable(true);
    	
		V_P_gender.getSelectionModel().selectFirst();
		V_P_state.getSelectionModel().selectFirst();
		
	}


}
